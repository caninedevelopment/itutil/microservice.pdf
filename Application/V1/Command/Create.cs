﻿using Application.PDF.V1.DTO;

namespace Monosoft.Service.PDF.V1.Command
{
    public class Create : ITUtil.Common.Command.GetCommand<ForPdfCreation, ForPdfOutput>
    {
        public Create()
            : base("Create a PDF from html")
        {
        }

        public ForPdfOutput Execute(ForPdfCreation input)
        {
            return Namespace.logic.CreatePdf(input);
        }
    }
}

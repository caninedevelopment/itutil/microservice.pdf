﻿namespace Monosoft.Service.PDF
{
    class Program
    {
        static void Main(string[] args)
        {
            ITUtil.Common.Console.Program.StartRabbitMq(new System.Collections.Generic.List<ITUtil.Common.Command.INamespace>()
            {
                new V1.Command.Namespace(),
            });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using Application.PDF.V1.DTO;
using NReco.ImageGenerator;
using NReco.PdfGenerator;

namespace Application.PDF.V1
{
    /// <summary>
    /// Units
    /// </summary>
    public static class Units
    {
        /// <summary>
        /// Converts millimeters (mm) to inches (in)
        /// </summary>
        /// <param name="millimeters">decimal millimeters</param>
        /// <returns>decimal</returns>
        public static decimal MillimetersToInches(decimal millimeters)
        {
            return millimeters / 25.4m;
        }

        /// <summary>
        /// Converts inches (in) to millimeters (mm)
        /// </summary>
        /// <param name="inches">decimal inches</param>
        /// <returns>decimal</returns>
        public static decimal InchesToMillimeters(decimal inches)
        {
            return inches * 25.4m;
        }

        /// <summary>
        /// Converts millimeters (mm) to pixels (px) based on dots per inch (dpi)
        /// </summary>
        /// <param name="millimeters">decimal millimeters</param>
        /// <param name="dpi">decimal dpi</param>
        /// <returns>decimal</returns>
        public static decimal MillimetersToPixels(decimal millimeters, decimal dpi)
        {
            return millimeters / 25.4m * dpi;
        }

        /// <summary>
        /// Converts pixels (px) to millimeters (mm) based on dots per inch (dpi)
        /// </summary>
        /// <param name="pixels">decimal pixels</param>
        /// <param name="dpi">decimal dpi</param>
        /// <returns>decimal</returns>
        public static decimal PixelsToMillimeters(decimal pixels, decimal dpi)
        {
            return pixels * 25.4m / dpi;
        }
    }
}

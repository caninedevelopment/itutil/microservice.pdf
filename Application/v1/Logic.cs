﻿// <copyright file="Logic.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Application.PDF.V1
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Printing;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using Application.PDF.V1.DTO;
    using NReco.ImageGenerator;
    using NReco.PdfGenerator;

    /// <summary>
    /// Logic
    /// </summary>
    public class Logic
    {
        private HtmlToPdfConverter pdfConverter = new HtmlToPdfConverter();

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        public Logic() // TODO move licenses to text file.
        {
            this.pdfConverter.License.SetLicenseKey(
                "PDF_Generator_Bin_Examples_Pack_250172112002",
                "FNKTmsKuTKLPMnlLt7IYjyp8s1AxrxMbE7MBQTML9DdGjN0mEIT23Skg1efYxo/XZGPF6rVr87TRkZxWeddeX9xnG3jJUlJ4o5rpRtmpTHBxDPxWyuoQdMWfxMufc8jYCeJtw7ZFoC4Q9opwM9gDPwd59lSqhXlNX1kl4rX+7fY=");

            NReco.ImageGenerator.License.SetLicenseKey(
                "Image_Generator_Src_Examples_Pack_250172117482",
                "FEo5HDTwjtJSBCPCakqIaXOkOoErz0UtPmbv/XsMB9+SZZDXY0kqEDtYhFQMPnEFyxkIXDQNZUpNdvXTrplD1nhhhr9znVn508X42xiAwQYN0Q3jRGPGFm70mvqZgPwuDpKlDCN2x2dIh9gp2TuJVeuGiihomsX8/c4OyJg0Rjc=");
        }

        private static readonly Dictionary<PaperKind, Size> PaperSizes = new Dictionary<PaperKind, Size> // Contains papersizes in Mm
        {
            { PaperKind.A2, new Size(width: 420, height: 594) },
            { PaperKind.A3, new Size(width: 297, height: 420) },
            { PaperKind.A3Rotated, new Size(width: 420, height: 297) },
            { PaperKind.A4, new Size(width: 210, height: 297) },
            { PaperKind.A4Rotated, new Size(width: 297, height: 210) },
            { PaperKind.A5, new Size(width: 148, height: 210) },
            { PaperKind.A5Rotated, new Size(width: 210, height: 148) },
        };

        /// <summary>
        /// CreatePdf
        /// </summary>
        /// <param name="forPdfCreation">ForPdfCreation forPdfCreation</param>
        /// <returns>ForPdfOutput</returns>
        public ForPdfOutput CreatePdf(ForPdfCreation forPdfCreation)
        {
            if (forPdfCreation == null)
            {
                throw new ArgumentNullException(nameof(forPdfCreation));
            }

            byte[] pdfBytes = null;
            if (forPdfCreation.body != null)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US"); // ensures that decimal point is . and not ,

                this.pdfConverter.PageHeaderHtml = forPdfCreation.header;
                this.pdfConverter.PageFooterHtml = forPdfCreation.footer;

                float marginInMm = 20;
                float headerHeightInMm = 0;
                float footerHeightInMm = 0;
                int dpi = 200; // 10. Workaround for wkhtmltopdf letter spacing issue (bad kerning). https://www.nrecosite.com/pdf_generator_net.aspx#faq10

                // PDF Dimensions.
                #region PDF Dimensions

                var paperSize = PaperSizes[PaperKind.A4];
                this.pdfConverter.PageWidth = paperSize.Width;
                this.pdfConverter.PageHeight = paperSize.Height;
                #endregion

                var widthMinusSideMargin = Math.Round(Units.MillimetersToPixels((decimal)(this.pdfConverter.PageWidth - (marginInMm * 2)), 96));

                if (this.pdfConverter.PageHeaderHtml != null)
                {
                    int headerHeight = GenerateImageAndReturnHeight(this.pdfConverter.PageHeaderHtml, (int)widthMinusSideMargin, dpi);
                    headerHeightInMm = (float)Units.PixelsToMillimeters(headerHeight, dpi);
                }

                if (this.pdfConverter.PageFooterHtml != null)
                {
                    var footerHeight = GenerateImageAndReturnHeight(this.pdfConverter.PageFooterHtml, (int)widthMinusSideMargin, dpi);
                    footerHeightInMm = (float)Units.PixelsToMillimeters(footerHeight, dpi);
                }

                this.pdfConverter.Margins = new PageMargins { Top = headerHeightInMm + marginInMm, Right = marginInMm, Bottom = footerHeightInMm + marginInMm, Left = marginInMm };
                string globalOptions = string.Empty;
                globalOptions += " --disable-smart-shrinking"; // !!IMPORTANT!! WYSIWYG.
                globalOptions += " --dpi " + dpi;
                globalOptions += " --encoding utf-8";

                this.pdfConverter.CustomWkHtmlArgs = globalOptions;
                pdfBytes = this.pdfConverter.GeneratePdf(forPdfCreation.body, forPdfCreation.cover);
            }

            ForPdfOutput pdfOutput = new ForPdfOutput();
            pdfOutput.pdf = pdfBytes.ToList();
            return pdfOutput;
        }

        private static int GenerateImageAndReturnHeight(string html, int width, int dpi)
        {
            // Convert DPI to Zoom and width;
            float zoomFactor = dpi / 96f; // 96 is the default DPI
            float virtualWidth = zoomFactor * width;
            HtmlToImageConverter imageConverter = new HtmlToImageConverter();
            imageConverter.WkHtmlToImageExeName = @".\wkhtmltopdf\wkhtmltoimage.exe";
            string globalOptions = string.Empty;
            globalOptions += " --quality 1";
            globalOptions += " --encoding utf-8";
            globalOptions += " --zoom " + zoomFactor;
            globalOptions += " --width " + Math.Ceiling(virtualWidth);
            imageConverter.CustomArgs = globalOptions;
            byte[] imageBytes = imageConverter.GenerateImage(html, ImageFormat.Jpeg);
            Image image = Image.FromStream(new MemoryStream(imageBytes));
            int heightInPx = image.Height;
            image.Dispose();

            // #region REMOVE
            // string fullImagePath = Path.Combine(baseDirectory, "out.jpg");
            // File.WriteAllBytes(fullImagePath, imageBytes);
            // #endregion
            return heightInPx;
        }
    }
}

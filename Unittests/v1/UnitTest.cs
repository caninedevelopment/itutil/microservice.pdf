﻿// <copyright file="UnitTest.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Unittests
{
    using Application.PDF.V1;
    using Application.PDF.V1.DTO;
    using NUnit.Framework;

    /// <summary>
    /// UnitTest
    /// </summary>
    [TestFixture]
    public class UnitTest
    {
        /// <summary>
        /// PdfGeneratePdfWithBody
        /// </summary>
        [Test]
        public void PdfGeneratePdfWithBody()
        {
            Logic logic = new Logic();

            ForPdfCreation forPdfInput = new ForPdfCreation
            {
                body = "<p>hello world</p>",
            };
            var res = logic.CreatePdf(forPdfInput);
            Assert.IsNotNull(res, "We should be able to create a new template");
            Assert.Greater(res.pdf.ToArray().Length, 0);
        }

        /// <summary>
        /// PdfGeneratePdfWithBodyAndHeader
        /// </summary>
        [Test]
        public void PdfGeneratePdfWithBodyAndHeader()
        {
            Logic logic = new Logic();

            ForPdfCreation forPdfInput = new ForPdfCreation
            {
                body = "<p>hello world</p>",
                header = System.IO.File.ReadAllText(@"..\..\..\htmlTemplates\header.html"),
            };
            var res = logic.CreatePdf(forPdfInput);
            Assert.IsNotNull(res, "We should be able to create a new template");
            Assert.Greater(res.pdf.ToArray().Length, 0);
        }
    }
}
